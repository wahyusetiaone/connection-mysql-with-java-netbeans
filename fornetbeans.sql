-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.21-1 - (Debian)
-- Server OS:                    Linux
-- HeidiSQL Version:             10.1.0.5464
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for fornetbeans
DROP DATABASE IF EXISTS `fornetbeans`;
CREATE DATABASE IF NOT EXISTS `fornetbeans` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `fornetbeans`;

-- Dumping structure for table fornetbeans.data
DROP TABLE IF EXISTS `data`;
CREATE TABLE IF NOT EXISTS `data` (
  `nama` varchar(50) DEFAULT NULL,
  `alamat` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- Dumping data for table fornetbeans.data: ~2 rows (approximately)
/*!40000 ALTER TABLE `data` DISABLE KEYS */;
REPLACE INTO `data` (`nama`, `alamat`) VALUES
	('Wahyu', 'Solo'),
	('Paijo', 'Wonogiri');
/*!40000 ALTER TABLE `data` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
